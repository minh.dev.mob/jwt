import {NextFunction, Request, Response,} from 'express'
import bcryptjs from 'bcryptjs';
import mongoose from 'mongoose';
import logging from '../config/logging';
import User from '../models/user';
import signJWT from '../functions/signJTW';
const NAMESPACE = "User"

const validateToken = (req: Request, res: Response, next: NextFunction) => {
    logging.info(NAMESPACE, "Token validation, user authorized")
    return res.status(200).json({
        message: "Authorized"
    })
};

const register = (req: Request, res: Response, next: NextFunction) => {
    let {username, password} = req.body;
    bcryptjs.hash(password, 10, (hashError, hash) => {
            if (hashError) {
                return res.status(401).json({
                    message: hashError.message,
                    error: hashError
                })
            }
            const _user = new User({
                _id :mongoose.Types.ObjectId(),
                username,
                password:hash
            })
        return _user
            .save()
            .then((user:any)=>{
                return res.status(200).json({
                    user
                })
            })
            .catch((error:any)=>{
                return res.status(500).json({
                    message:error.message,
                    error
                })
            })
        }
    )
}

const login = (req: Request, res: Response, next: NextFunction) => {
    let { username, password } = req.body;

    User.find({ username })
        .exec()
        .then((users:any) => {
            if (users.length !== 1) {
                return res.status(401).json({
                    message: 'Unauthorized'
                });
            }

            bcryptjs.compare(password, users[0].password, (error, result) => {
                if (error) {
                    return res.status(401).json({
                        message: 'Password Mismatch'
                    });
                } else if (result) {
                    signJWT(users[0], (_error:any, token:any) => {
                        if (_error) {
                            return res.status(500).json({
                                message: _error.message,
                                error: _error
                            });
                        } else if (token) {
                            return res.status(200).json({
                                message: 'Auth successful',
                                token: token,
                                user: users[0]
                            });
                        }
                    });
                }
            });
        })
        .catch((err:any) => {
            console.log(err);
            res.status(500).json({
                error: err
            });
        });
}

const getAllusers = (req: Request, res: Response, next: NextFunction) => {
    User.find()
        .select('-password')
        .exec()
        .then((users:any) => {
            return res.status(200).json({
                users: users,
                count: users.length
            });
        })
        .catch((error:any) => {
            return res.status(500).json({
                message: error.message,
                error
            });
        });
}
export default {validateToken, register, login, getAllusers}
